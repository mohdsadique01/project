import React from 'react'
import Logo01 from "./oryen/Oryen_Logo-01.png"
import Icon from "./oryen/logo_icon.png"
import "./Footer.css"
function Footer() {
  return (
    <div className='footer'>
      <div className='img_2'> <img src={Logo01} className='logo_2' alt='' /> </div>
      <div className='footer-side-box_2'>
        <div className='main-box_2'><img src={Icon} className='icon' alt='' /><p className='box_2'> $0.003</p></div>
        <div className='box_3'> <span className="p-10">BUY ORYEN</span></div>
      </div>
    </div>
  )
}

export default Footer
