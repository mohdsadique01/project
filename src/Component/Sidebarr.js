import React from 'react'
import './Sidebarr.css'
function Sidebarr() {
  return (
    <div className='sidebar'>
      <div className='dashbord'>
        {/* <img className='image_3' src='https://png.pngtree.com/png-clipart/20191120/original/pngtree-outline-home-icon-and-symbol-isolated-png-image_5045551.jpg' alt=''/> */}
        <i className="fa fa-home color pr-8" aria-hidden="true"></i>
        <span className='color'>Dashboard</span>
      </div>
      <br></br>

      <div>
      <div className='dashbord'>
      <i className="fa fa-exchange pr-8" aria-hidden="true"></i>
        <span className='fontsixe'>Exchange</span>
      </div>
      <div className='dashbord'>
      <i className="fa fa-calculator pr-8" aria-hidden="true"></i>
        <span className='fontsixe'>Calculator</span>
      </div>
      
      <div className='dashbord'>
      <i className="fa fa-universal-access pr-8" aria-hidden="true"></i>
        <span className='fontsixe'>Account</span>
      </div>
      </div>

   

      <div>
      <p className='discp'>ABOUT</p>
        <div className='dashbord'>
        <i className="fa fa-address-book pr-8" aria-hidden="true"></i>
        <p className='fontsixe'>Contact</p>
      </div>
      <div className='dashbord'>
      <i className="fa fa-address-book pr-8" aria-hidden="true"></i>
        <p className='fontsixe'>Community</p>
      </div>
      <div className='dashbord'>
      <i className="fa fa-address-book pr-8" aria-hidden="true"></i>
        <p className='fontsixe'>Oryen Token</p>
      </div>
     </div>


    
     <div>
     <p className='discp'>HELP</p>
     <div className='dashbord'>
     <i className="fa fa-file-text pr-8" aria-hidden="true"></i>        <p className='fontsixe'>Documentation</p>
      </div>
      <div className='dashbord'>
      <i className="fa fa-file-code-o pr-8" aria-hidden="true"></i>
        <p className='fontsixe'>FAQ</p>
      </div>
     </div>

     <div className='container--4'>
          <div className='social'> SOCIAL CONNECTION</div>
          <div className='box-6'>
            
            <i className="fa fa-facebook pr-9" aria-hidden="true"></i>
            <i className="fa fa-instagram pr-9" aria-hidden="true"></i>
            <i className="fa fa-twitter pr-9" aria-hidden="true"></i>
            <i className="fa fa-telegram pr-9" aria-hidden="true"></i>
            <i className="fa-brands fa-discord pr-9"  aria-hidden="true"></i>
           
          </div>
        </div>


    </div>
  )
}

export default Sidebarr
