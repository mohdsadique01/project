import React from 'react'
import Image from './oryen/single_glow.png'
import Glow from './oryen/Fixed_APY_glow.png'
import img from './oryen/circle.png'
import logo from './oryen/logo_icon.png'
import'./Main.css'


function Main() {
  return (
    <div className='main1'>
       <div className='dash-text'> DASHBORD</div>
       <div className='box-1'>
        <div className='box-21'><span className='font'>Marketcap</span> <br></br><span className='rate'>$11.97M</span></div>
        <div className='box-21'><span className='font'>Circulating Supply</span> <br></br><span className='rate'>38.35B</span></div>
        <div className='box-21'><span className='font'>Holders</span>  <br></br><span className='rate'>82K</span></div>
        <div className='box-21'><span className='font'>Oryen price</span>  <br></br><span className='rate'>$0.0003</span></div>
       </div >



       <div className='container'>
        <div className='main-box'>
          <div className='box-3'>
                    <div className='sml' style={{ backgroundImage:`url(${Image})`  }}><span className='liq'>Liquidity</span><br></br> <span className='bld'>$1.61M</span></div>
            <p className='perc'>-0.33%</p>
          </div>
       </div>

        <div className='main-box ml-10'>
        <div className='box-3'>
           <div className='sml' style={{ backgroundImage:`url(${Image})`  }}> <span className='liq'>Treasury</span> <br></br><span className='bld'>$8.34M</span> </div>
            <p className='perc'>-0.33%</p>
          </div>
        </div>
       </div>


       <div className='container-2'>
        <div className='flex' style={{ backgroundImage:`url(${Glow})`  }}>
          <div className='fixed'>Fixed APY</div>
          <div className='numb'>102 483.58%</div>
          <div className='detail'>A simple Buy-Hold Earn System.</div>
        </div>
        <div>
          <img className='img' src= {img} alt=''/>
        </div>
        
       </div>


       <div className='container-5'>
        <div className='container-3'>
          <div className='img-box'>
            <img className='image' src={logo} alt='' />
          </div>
          <div className='box-4'>
          <div className='smallbox-2'>Still got question</div>
          <div className='smallbox'><p className='clm'>More detail</p></div>
          </div>
        </div>

        <div className='container-4'>
          <div className='connect'> Connect with us</div>
          <div className='box-6'>
          <i className="fa fa-facebook pr-9" aria-hidden="true"></i>
            <i className="fa fa-instagram pr-9" aria-hidden="true"></i>
            <i className="fa fa-twitter pr-9" aria-hidden="true"></i>
            <i className="fa fa-telegram pr-9" aria-hidden="true"></i>
            <i className="fa-brands fa-discord pr-9"  aria-hidden="true"></i>
          </div>
        </div>

       </div>

       
    </div>
  )
}

export default Main
