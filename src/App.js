import React from 'react';
import './App.css';
import Footer from './Component/Footer';
import Nav from './Component/Nav';
import Main from './Component/Main';


import Sidebarr from './Component/Sidebarr';

function App() {
  return (
    <div className="App">
    
    
     <Nav />
    

     <div className='app'>
    <Sidebarr />
    <Main />
    </div>


    <Footer /> 
    </div>
  );
}

export default App;
